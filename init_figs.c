/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_figs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/21 15:46:35 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/23 18:08:51 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_fig	*ft_new_fig(void)
{
	int		i;
	t_fig	*elem;

	i = 0;
	if (!(elem = (t_fig *)malloc(sizeof(*elem))))
		return (NULL);
	while (i < 4)
	{
		elem->x[i] = 0;
		elem->y[i] = 0;
		i++;
	}
	elem->index = 0;
	elem->fig_nb = 0;
	elem->square = 0;
	elem->next = NULL;
	return (elem);
}

t_fig	*ft_fill_fig(char **s)
{
	int		i;
	int		j;
	int		k;
	t_fig	*list;
	t_fig	*fig;

	i = 0;
	j = 0;
	k = 0;
	fig = ft_new_fig();
	list = fig;
	while (s[i])
	{
		while (s[i][j])
		{
			if (s[i][j] == '#')
			{
				fig->x[k] = j / 4;
				fig->y[k] = j % 4;
				k++;
			}
			j++;
		}
		fig->index = i + 1;
		fig->next = ft_new_fig();
		fig = fig->next;
		j = 0;
		k = 0;
		i++;
	}
	ft_get_lstnb(list, i);
	ft_init_list(list);
	ft_memdel((void **)s);
	return (list);
}

void	ft_get_lstnb(t_fig *list, int i)
{
	t_fig *fig;

	fig = list;
	while (fig->next)
	{
		fig->fig_nb = i;
		fig = fig->next;
	}
}

void	ft_init_list(t_fig *list)
{
	int		i;
	int		min_x;
	int		min_y;
	t_fig	*fig;

	fig = list;
	while (fig->next)
	{
		i = 0;
		min_x = fig->x[i];
		min_y = fig->y[i];
		while (i < 4)
		{
			if (fig->x[i] < min_x)
				min_x = fig->x[i];
			if (fig->y[i] < min_y)
				min_y = fig->y[i];
			i++;
		}
		i = 0;
		while (i < 4)
		{
			fig->x[i] -= min_x;
			fig->y[i] -= min_y;
			i++;
		}
		fig = fig->next;
	}
}
