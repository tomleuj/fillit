/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/17 18:05:15 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/23 18:28:46 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include "./library/libft.h"

typedef struct		s_fig
{
	int				x[4];
	int				y[4];
	int				index;
	int				fig_nb;
	int				square;
	struct s_fig	*next;
}					t_fig;

char				**ft_alloc_grid(char **grid, int square);
void				ft_init_grid(char **grid, int square);

t_fig				*open_read_cpy(char *argv);
char				*ft_read_check(char *s, int fd, int *fig_nb);
int					ft_final_check(char *s);
void				ft_erase_nextline(char*s);

char				**ft_solve(t_fig *list);
void				ft_find_square(t_fig *list);
int					ft_check_grid(char **grid, t_fig *list, int x, int y);
void				ft_fill_grid(char **grid, t_fig *list, int x, int y);
int					ft_backtrack(char **grid, t_fig *list);
void				ft_erase_grid(char **grid, int fig);

int					check(char *buff, int ret);
int					check_touch(char *buff);
int					check_good_char(char *buff);
int					check_nb_block(char *buff);
int					check_separator(char *buff, size_t ret);

void				ft_putgrid(char **grid);

t_fig				*ft_new_fig();
t_fig				*ft_fill_fig(char **s);
void				ft_get_lstnb(t_fig *list, int i);
void				ft_init_list(t_fig *list);

#endif
