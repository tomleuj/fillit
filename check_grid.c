/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_grid.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 15:24:22 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/23 15:27:43 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	ft_find_square(t_fig *list)
{
	t_fig	*fig;
	int		square;

	square = 0;
	fig = list;
	while (square * square < (4 * list->fig_nb))
		square++;
	while (fig->next)
	{
		fig->square = square;
		fig = fig->next;
	}
}

char	**ft_alloc_grid(char **grid, int square)
{
	int x;

	x = 0;
	if (!(grid = (char **)malloc(sizeof(char*) * (square + 2))))
		return (NULL);
	while (x <= square)
	{
		if (!(grid[x] = (char *)malloc(sizeof(char) * (square + 2))))
			return (NULL);
		x++;
	}
	ft_init_grid(grid, square);
	return (grid);
}

void	ft_init_grid(char **grid, int square)
{
	int x;
	int y;
	int z;

	x = 0;
	y = 0;
	z = 0;
	while (x < square)
	{
		while (y < square)
		{
			grid[x][y] = '.';
			y++;
		}
		grid[x][square] = '\n';
		grid[x][square + 1] = '\0';
		y = 0;
		x++;
	}
	while (z <= square)
		grid[x][z++] = '\n';
	grid[x][z] = '\0';
	grid[x + 1] = NULL;
}
