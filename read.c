/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_grid.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aparrot <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/19 09:38:52 by aparrot           #+#    #+#             */
/*   Updated: 2016/11/23 17:58:07 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_fig	*open_read_cpy(char *argv)
{
	int		fd;
	int		fig_nb;
	char	*s;
	t_fig	*list;

	s = NULL;
	list = NULL;
	fig_nb = 0;
	if ((fd = open(argv, O_RDONLY)) == -1)
	{
		ft_putstr("error\n");
		return (NULL);
	}
	if (!(s = ft_read_check(s, fd, &fig_nb)) || fig_nb > 26
			|| fig_nb == 0)
	{
		ft_putstr("error\n");
		return (NULL);
	}
	ft_erase_nextline(s);
	if (!(list = ft_fill_fig(ft_strsplit(s, '\n'))))
		return (NULL);
	free(s);
	s = NULL;
	close(fd);
	return (list);
}

char	*ft_read_check(char *s, int fd, int *fig_nb)
{
	char	*buff;
	size_t	ret;
	size_t	i;

	i = 0;
	if (!(buff = ft_strnew(21)))
		return (NULL);
	if (!(s = ft_strnew(0)))
		return (NULL);
	s[0] = '\0';
	while ((ret = read(fd, buff, 21)) >= 20)
	{
		buff[ret] = '\0';
		if (check(buff, ret))
			return (NULL);
		s = ft_realloc(s, buff, ret + i);
		i += ret;
		(*fig_nb)++;
	}
	free(buff);
	if (ft_final_check(s))
		return (NULL);
	return (s);
}

int		ft_final_check(char *s)
{
	size_t	n;

	n = ft_strlen(s);
	if (s[n - 1] == '\n' && s[n - 2] == '\n')
		return (6);
	return (0);
}

void	ft_erase_nextline(char *s)
{
	int i;
	int j;

	i = 0;
	while (s[i])
	{
		if (s[i] == '\n')
		{
			j = i;
			while (s[j + 1] != '\0')
			{
				s[j] = s[j + 1];
				j++;
			}
			j = 0;
		}
		i++;
	}
}
