/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/17 22:17:45 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/23 15:21:43 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		main(int ac, char **av)
{
	t_fig	*list;
	char	**grid;

	if (ac != 2)
	{
		ft_putstr_fd("usage: fillit input_file\n", 2);
		return (1);
	}
	if (!(list = open_read_cpy(av[1])))
		return (1);
	if (!(grid = ft_solve(list)))
		return (1);
	ft_putgrid(grid);
	return (0);
}

void	ft_putgrid(char **grid)
{
	int	a;

	a = 0;
	while (grid[a + 1])
	{
		ft_putstr(grid[a]);
		a++;
	}
}
