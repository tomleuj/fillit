/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 15:19:29 by tlejeune          #+#    #+#             */
/*   Updated: 2016/12/08 19:33:33 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		check(char *buff, int ret)
{
	if (check_touch(buff))
		return (check_touch(buff));
	if (check_good_char(buff))
		return (check_good_char(buff));
	if (check_nb_block(buff))
		return (check_nb_block(buff));
	if (check_separator(buff, ret))
		return (check_separator(buff, ret));
	return (0);
}

int		check_touch(char *buff)
{
	int		i;
	int		count;

	i = 0;
	count = 0;
	while (buff[i])
	{
		if (buff[i] == '#' && buff[i + 1] == '#' && i < 20)
			count++;
		if (buff[i] == '#' && buff[i - 1] == '#' && i >= 0)
			count++;
		if (buff[i] == '#' && buff[i + 5] == '#' && i < 20)
			count++;
		if (buff[i] == '#' && buff[i - 5] == '#' && i >= 0)
			count++;
		i++;
	}
	if (count == 6 || count == 8)
		return (0);
	return (1);
}

int		check_good_char(char *buff)
{
	int	i;

	i = 0;
	while (i < 21)
	{
		if (i % 5 == 4)
		{
			if (buff[i] != '\n')
				return (2);
		}
		if (((i % 5) < 4) && (i != 20))
		{
			if (buff[i] != '#' && buff[i] != '.')
				return (3);
		}
		i++;
	}
	return (0);
}

int		check_nb_block(char *buff)
{
	int		i;
	int		count;

	i = 0;
	count = 0;
	while (buff[i])
	{
		if (buff[i] == '#')
			count++;
		if (count > 4)
			return (4);
		i++;
	}
	return (0);
}

int		check_separator(char *buff, size_t ret)
{
	if (ret == 21 && buff[ret - 1] != '\n')
		return (5);
	return (0);
}
