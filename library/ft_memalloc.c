/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/12 01:04:21 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/12 01:10:09 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	unsigned char *space;

	if (!(space = (unsigned char*)malloc(sizeof(*space) * size)))
		return (NULL);
	ft_bzero(space, size);
	return (space);
}
