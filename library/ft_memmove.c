/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 13:43:05 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/14 18:31:02 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t n)
{
	unsigned char	*src_p;
	unsigned char	*dst_p;
	size_t			i;

	src_p = (unsigned char *)src;
	dst_p = (unsigned char *)dst;
	i = 0;
	if (src < dst)
	{
		while (n--)
			dst_p[n] = src_p[n];
	}
	else if (src > dst)
	{
		while (i < n)
		{
			dst_p[i] = src_p[i];
			i++;
		}
	}
	return (dst);
}
