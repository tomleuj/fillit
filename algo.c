/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   algo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlejeune <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/18 20:55:30 by tlejeune          #+#    #+#             */
/*   Updated: 2016/11/23 15:26:54 by tlejeune         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char	**ft_solve(t_fig *list)
{
	int		square;
	char	**grid;

	grid = NULL;
	ft_find_square(list);
	if (!(grid = ft_alloc_grid(grid, list->square)))
		return (NULL);
	square = list->square;
	while (!(ft_backtrack(grid, list)))
	{
		square++;
		ft_memdel((void**)grid);
		grid = ft_alloc_grid(grid, square);
	}
	return (grid);
}

int		ft_check_grid(char **grid, t_fig *list, int x, int y)
{
	if ((grid[x + list->x[0]][y + list->y[0]] == '.') &&
		(grid[x + list->x[1]][y + list->y[1]] == '.') &&
		(grid[x + list->x[2]][y + list->y[2]] == '.') &&
		(grid[x + list->x[3]][y + list->y[3]] == '.'))
		return (1);
	return (0);
}

void	ft_fill_grid(char **grid, t_fig *list, int x, int y)
{
	int i;

	i = 0;
	while (i < 4 && grid[x][y])
	{
		(grid[x + list->x[i]][y + list->y[i]] = list->index + 64);
		i++;
	}
}

int		ft_backtrack(char **grid, t_fig *list)
{
	int x;
	int y;

	x = 0;
	y = 0;
	if (list->next == NULL)
		return (1);
	while (grid[x + 1])
	{
		if (ft_check_grid(grid, list, x, y))
		{
			ft_fill_grid(grid, list, x, y);
			if ((ft_backtrack(grid, list->next)))
				return (1);
			ft_erase_grid(grid, list->index);
		}
		if (grid[x][y + 1])
			y++;
		else
		{
			y = 0;
			x++;
		}
	}
	return (0);
}

void	ft_erase_grid(char **grid, int fig)
{
	int		x;
	int		y;
	char	letter;

	x = 0;
	y = 0;
	letter = fig + 64;
	while (grid[x])
	{
		while (grid[x][y])
		{
			if (grid[x][y] == letter)
				grid[x][y] = '.';
			y++;
		}
		y = 0;
		x++;
	}
}
